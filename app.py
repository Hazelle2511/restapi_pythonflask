from flask import Flask, make_response, request, jsonify
from flask_mongoengine import MongoEngine
from api_constants import mongo_password




app = Flask(__name__)
database_name = "api"
# app.config["MONGO_URI"] = "mongodb://localhost:27017/veilles"
password = mongo_password

DB_URI = "mongodb+srv://admin:{}@pythoncluster.xbttp.mongodb.net/{}?retryWrites=true&w=majority".format(
  password, database_name)
app.config["MONGODB_HOST"] = DB_URI

db = MongoEngine()
db.init_app(app)



class Veilles(db.Document):
   veille_id = db.IntField()
   name = db.StringField()
   author = db.StringField()
   link = db.StringField()
 

   def to_json(self):
        return{
            "veille_id": self.veille_id,
            "name": self.name,
            "author": self.author,
            "link": self.link

           
        }

@app.route('/api/db_populate', methods = ['POST'])
def db_populate():
    veille1 = Veilles(veille_id=1,name="Symphony", link="11-22-22", author="Shivansh")
    veille2 = Veilles(veille_id=2,name="Laravel", link="11-22-22", author="Manu")
    veille1.save()
    veille2.save()
    return make_response("", 201)


"""
@api {get} /api/v1/topics Display veilles information


@apiSuccess {String} name Name of the Veille.
@apiSuccess {String} author  Author of the Veille.
@apiSuccess {String} Link  Link of the Veille.
"""
@app.route('/api/v1/topics', methods = ['GET', 'POST'])
def api_veilles():
    if request.method == "GET":
          veilles = []
          for veille in Veilles.objects:
              veilles.append(veille)
          return make_response(jsonify(veilles), 200)
    elif request.method == "POST":
        content = request.json
        book = Veilles(veille_id=content['veille_id'],
        name=content['name'], link=content['link'], author=content['author'])

@app.route('/api/v1/topics/<veille_id>', methods = ['GET','PUT', 'DELETE'], )
def api_each_veille(veille_id):
      if request.method == "GET":
         veille_obj = Veilles.objects(veille_id=veille_id).first()
         if veille_obj:
             return make_response(jsonify(veille_obj.to_json()), 200)
         else:
             return make_response("", 404)
      elif request.method == "PUT":
            
            content = request.json
            veille_obj = Veilles.objects(veille_id=veille_id).first()
            veille_obj.update(author=content['author'], name=content['name'])
            return make_response("", 204)
      elif request.method == "DELETE":
            veille_obj = Veilles.objects(veille_id=veille_id).first()
            veille_obj.delete()
            return make_response("", 204)


if __name__ == '__main__':
    app.run()






