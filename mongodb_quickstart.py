import mongoengine as db
from api_constants import mongo_password
# from pymongo import database

database_name = "quickstart"
password = mongo_password
DB_URI = "mongodb+srv://admin:{}@pythoncluster.xbttp.mongodb.net/{}?retryWrites=true&w=majority".format(
  password, database_name
)
db.connect(host=DB_URI)
# # from flask import Flask
# # from flask_mongoengine import MongoEngine
# # # from api_constants import mongodb_password

# # app = Flask(__name__)
# # database_name = "quickstart"
# # app.config["MONGO_URI"] = "mongodb://localhost:27017/veilles"
# # # DB_URI = "mongodb+srv://admin:k2JU8B22fjHAR7z@cluster0.b8jly.mongodb.net/veilles?retryWrites=true&w=majority"
# # # app.config("MONGODB_HOST") = DB_URI
# # db = MongoEngine()
# # db.init_app(app)


class Book(db.Document):
   book_id = db.IntField()
   name = db.StringField()
   author = db.StringField()
 

   def to_json(self):
        return{
            "book_id": self.book_id,
            "name": self.name,
            "author": self.author,
           
        }
# print("\nCreate a Book")
# book = Book(book_id = 1,
# name = "Game of Thrones",
# author = "George R.R Martin")
# book.save()

# print("\nFetch a book")
book = Book.objects(book_id=1).first()
# print(book.to_json())

# print("\nUpdate a book")
# book.update(name="Harry Potter", author="J.K Rowling")
# print(book.to_json())

print("\nAdd another Book")
book = Book(book_id=2, name="The Alchemist", author="Paulo Coelho"
)

book.save()

# print("\nFetch all books")
# books = []
# for book in Book.objects():
#     books.append(book.to_json())
# print(books)

# print("\nFind books whose name contains The")
# books = []
# for book in Book.objects(name__contains="The"):
#     books.append(book.to_json())
# print(books)

# print("\nHow many books are in a collection?")
# print(Book.objects.count())

# print("\nOrder bu author field")
# books = []
# for book in Book.objects().order_by('author'):
#     books.append(book.to_json())
# print(books)

# print("\nDelete a book")
# book = Book.objects(book_id=2).first()
# book.delete()
# print(Book.objects.count())

# print("\nDelete all books in a collection")
# for book in Book.objects():
#     book.delete()
# print(Book.objects.count())

# # @app.route('/api/db_populate', methods = ['POST'])
# # def db_populate():
# #     veille1 = Veilles(veille_id=1,name="Symphony", date="11-22-22", presentor="Shivansh")
# #     veille2 = Veilles(veille_id=2,name="Laravel", date="11-22-22", presentor="Manu")
# #     veille1.save()
# #     veille2.save()

# # @app.route('/api/veilles', methods = ['GET', 'POST'])
# # def api_veilles():
# #     pass

# # @app.route('/api/veiles/<veille_id>', methods = ['GET','PUT', 'DELETE'], )
# # def api_each_veille(veille_id):
# #     pass

# # if __name__ == '__main__':
# #     app.run()

# from flask import Flask, escape, request

# app = Flask(__name__)

# @app.route('/')
# def hello():
#     name = request.args.get("name", "World")
#     return f'Hello, {escape(name)}!'

# from flask import Flask

# from flask_pymongo import PyMongo

# from bson.json_util import dumps

# from bson.objectid import ObjectId

# from flask import jsonify, request

# from werkzeug.security import generate_password_hash,check_password_hash

# app = Flask(__name__)
# app.secret_key = "secretkey"
# app.config["MONGO_URI"] = "mongodb://localhost/veilles"

# mongo = PyMongo(app)

# @app.route("/")
# def home_page():
#     online_users = mongo.db.users.find({"online": True})
#     return render_template("index.html",
#         online_users=online_users)

# @app.route('/add', methods=['POST'])

# def add_veille():
    
#     _json = request.json
#     _name = _json['name']
#     _date = _json['date']
#     _presentor = _json['presentor']

#     if _name and _date and _presentor and request.method == 'POST':

#         id = mongo.db.veilles.insert({'name':_name, 'date':_date, 'presentor':_presentor})
#         resp = jsonify("Veille added successfully")

#         resp.status.code = 200

#         return resp
#     else:
#         return not_found()




# @app.errorhandler(404)
# def not_found(error=None):
#     message = {
#         'status': 404,
#         'message': 'Not Found ' + request.url
#     }
#     resp = jsonify(message)
#     resp.status_code = 404

#     return resp

# if __name__ == "__main__":
#     app.run(debug=True, port=4000)


# from flask import Flask, request
# from flask_pymongo import PyMongo

# app = Flask(__name__)
# app.config["MONGO_URI"] = "mongodb://localhost/veilles"
# mongo = PyMongo(app)

# @app.route('/add', methods=['POST'])
# def create_veille():
#     #Receiving data
#     name = request.json['name']
#     date = request.json['date']
#     presentor = request.json['presentor']

#     if name and date and presentor:
#         id = mongo.db.veilles.insert(
#             {'name': name, 'date': date, 'presentor': presentor}
#         )
#         response = {
#             'id': str(id),
#             'name': name,
#             'date': date,
#             'presentor': presentor
#         }
#         return response
#     else:
#         {'message': 'received'}


#     # print(request.json)
   
#     return{'message': 'received'}

# if __name__ == "__main__":
#     app.run(debug=True)




# config = {
 
#   "apiKey": "AIzaSyAHCglrCt_RSNhs6bnWWDF_p0LO7AN3vXo",
#   "authDomain": "restapi-a0edb.firebaseapp.com",
#   "databaseURL": "https://restapi-a0edb-default-rtdb.europe-west1.firebasedatabase.app",
#   "projectId": "restapi-a0edb",
#   "storageBucket": "restapi-a0edb.appspot.com",
#   "messagingSenderId": "447458234498",
#   "appId": "1:447458234498:web:855ada9b2b6e2443c8249b"



# }

# firebase = pyrebase.initialize_app(config)

# db = firebase.database()

# # db.child("names").push({"name": "Magento", "presentor": "Manon"})
# from flask import *

# app = Flask(__name__)

# @app.route('/', methods=['GET', 'POST'])
# def basic():
#     if request.method == 'POST':
#         name = request.form['name']
#         db.child("veille").push(name)
#         veille = db.child("veille").get()
#         return todo.val()
# if __name__ == '__main__':
#     app.run(debug=True)

